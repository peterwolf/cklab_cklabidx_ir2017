#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.filters.lower_case_filter import LowerCaseFilter


class LowerCaseFilterTest(unittest.TestCase):

    def test_lowercase_filter(self):

        self.filter = LowerCaseFilter()

        self.assertEqual(self.filter.filter(["AA", "Bb", "cc"]), ["aa", "bb", "cc"])
        self.assertEqual(self.filter.filter(["AbCdEf", "012345"]), ["abcdef", "012345"])


if __name__ == '__main__':
    unittest.main()
