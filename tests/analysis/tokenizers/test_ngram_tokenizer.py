#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import unittest
from cklabidx.analysis.tokenizers.ngram_tokenizer import NGramTokenizer


class NGramTokenizerTest(unittest.TestCase):

    def test_ngram_tokenizer(self):

        self.tokenizer = NGramTokenizer(1, 1)
        self.assertEqual(self.tokenizer.generate_tokens("abc"), ["a", "b", "c"])
        self.assertEqual(self.tokenizer.generate_tokens("中文"), ["中", "文"])
        self.assertEqual(self.tokenizer.generate_tokens("中文abc"), ["中", "文", "a", "b", "c"])

        self.tokenizer = NGramTokenizer(1, 2)
        self.assertEqual(self.tokenizer.generate_tokens("abc"), ["a", "b", "c", "ab", "bc"])
        self.assertEqual(self.tokenizer.generate_tokens("中文"), ["中", "文", "中文"])
        self.assertEqual(self.tokenizer.generate_tokens("中文abc"),
                         ["中", "文", "a", "b", "c", "中文", "文a", "ab", "bc"])


if __name__ == '__main__':
    unittest.main()
