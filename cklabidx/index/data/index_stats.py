#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.index.data.base_index_data import BaseIndexData


class DocStats(BaseIndexData):
    """
    Data structure for Document Stats.

    Object format for data_dict:
    {
        # Store total token counts of a document
        "field1": {
            "doc1": total term counts,
            "doc2": total term counts,
            "doc3": total term counts,
        },
        "field2": {
            "doc4": total term counts,
            "doc5": total term counts,
            "doc6": total term counts,
        }, ...
    }

    """
    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = "stats_doc.json"
        self.file_name_pickle = "stats_doc.pickle"

    def add_document(self, doc_id, field, term_list):

        # Initialize document
        if field not in self.data_dict:
            self.data_dict[field] = {}

        if doc_id not in self.data_dict[field]:
            self.data_dict[field][doc_id] = 0

        # Update stats data
        self.data_dict[field][doc_id] += len(term_list)

    def get_stats_by_doc(self, field, doc_id):
        if field in self.data_dict and doc_id in self.data_dict[field]:
            return self.data_dict[field][doc_id]
        else:
            return 0


class FieldStats(BaseIndexData):
    """
    Data structure for Field Stats.

    Object format for data_dict:
    {
        # Store total token counts of a field
        "field1": total term counts,
        "field2": total term counts,
        ...
    }

    """

    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = "stats_field.json"
        self.file_name_pickle = "stats_field.pickle"

    def add_document(self, doc_id, field, term_list):

        # Initialize fields
        if field not in self.data_dict:
            self.data_dict[field] = 0

        # Update stats data
        self.data_dict[field] += len(term_list)

    def get_stats_by_field(self, field):
        if field in self.data_dict:
            return self.data_dict[field]
        else:
            return 0
