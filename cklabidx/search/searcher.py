#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from abc import ABCMeta, abstractmethod

from cklabidx.search.collector import DocumentCollector


class Searcher(metaclass=ABCMeta):
    """
    Searcher class to handle the search operation.
    """
    def __init__(self, index_object, scorer_object):
        self.index = index_object
        self.scorer = scorer_object
        self.scorer.set_index_object(index_object)

    @abstractmethod
    def search(self, query_object):
        """
        Do the search job!
        :param query_object:
        :return:
        """
        pass


class DocumentSearcher(Searcher):
    """
    Document searcher,
    return a Document Collector
    """
    def search(self, query_object):
        """
        Do the search job!
        Search terms in given query object in Index.
        :param query_object:
        :return:
        """

        self.scorer.set_query_object(query_object)

        doc_collector = DocumentCollector()

        # Implement boolean model.
        # Collect document hits in index with term exists.
        for field in query_object.query_dict:
            # Initialize set for each field!
            hit_docs_set = set()

            term_list = []
            for term in query_object.query_dict[field]:
                hit_doc_list = self.index.search(field, term)

                # Update hit document list, call once for each term
                # **Required for doc_coord() value**
                self.scorer.update_hit_doc_list(field, term, hit_doc_list)

                # Collect all possible documents with terms
                hit_docs_set.update(hit_doc_list)
                term_list.append(term)

            # Calculate scores for each doc hits
            # Store document scores by fields.
            hit_scores = self.scorer.get_scores(field, term_list, hit_docs_set)
            doc_collector.set_document_scores(field, hit_scores)

        return doc_collector


class PassageSearcher(Searcher):
    """
    Passage searcher
    """
    def search(self, query_object):
        """
        Do the search job!
        Search terms in given query object in Index.
        :param query_object:
        :return:
        """
        self.scorer.set_query_object(query_object)

        raise NotImplementedError
