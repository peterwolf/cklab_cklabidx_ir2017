#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from abc import ABCMeta, abstractmethod


class Tokenizer(metaclass=ABCMeta):
    """
    Base Tokenizer
    """

    @abstractmethod
    def generate_tokens(self, text):
        """
        Null Tokenizer
        :param text: Text input
        :return: Original text
        """

        return [text]
